package com.avast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ahsan on 26/11/14.
 * Reference http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/
 */

    public class EmailValidator {

        private Pattern pattern;
        private Matcher matcher;

        private static final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        public EmailValidator() {
            pattern = Pattern.compile(EMAIL_PATTERN);
        }

        /**
         * Validate str with regular expression
         *
         * @param str
         *            str for validation
         * @return true valid email, false invalid email
         */
        public boolean validate(final String str) {

            matcher = pattern.matcher(str);
            return matcher.matches();

        }
    }

